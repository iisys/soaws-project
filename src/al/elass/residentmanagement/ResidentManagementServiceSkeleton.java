/**
 * ResidentManagementServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */
package al.elass.residentmanagement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.axis2.context.ServiceContext;
import org.apache.axis2.service.Lifecycle;

/**
 * ResidentManagementServiceSkeleton java skeleton for the axisService
 */
public class ResidentManagementServiceSkeleton {

	private final static String HOSTNAME = "127.0.0.1";
	private final static String PORT = "5432";
	private final static String USERNAME = "postgres";
	private final static String PASSWORD = "root";
	private final static String DATABASE = "soaws";

	private static Connection connection;

	private static Connection getConnection() {
		if (connection == null) {
			try {
				Class.forName("org.postgresql.Driver");
			} catch (ClassNotFoundException e) {
				System.out.println("Error loading driver: " + e);
			}

			String url = "jdbc:postgresql://" + HOSTNAME + ":" + PORT + "/" + DATABASE;

			try {
				connection = DriverManager.getConnection(url, USERNAME, PASSWORD);
			} catch (SQLException e) {
				e.printStackTrace();
				// System.exit(1);
				return null;
			}
		}
		return connection;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getResidents
	 * @return getResidentsResponse
	 */

	public GetResidentsResponse getResidents(GetResidents getResidents) {
		try {
			GetResidentsResponse response = new GetResidentsResponse();
			ResidentList list = new ResidentList();

			PreparedStatement st = getConnection().prepareStatement("SELECT * FROM residents ORDER BY roomnumber ASC");
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				list.addResident(populateResident(rs));
			}
			response.setResidentList(list);
			return response;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param updateInfo
	 * @return updateInfoResponse
	 * @throws NoSuchResident
	 */

	public UpdateInfoResponse updateInfo(UpdateInfo updateInfo) throws NoSuchResident {
		
		String name = updateInfo.getResidentName();
		String info = updateInfo.getInfo().getInfo();
		
		try {
			PreparedStatement st = getConnection().prepareStatement(
					  "UPDATE residents SET info = ? WHERE name = ?"
			);
	        st.setString(1, info);
	        st.setString(2, name);
	        st.execute();
	        if (st.getUpdateCount() < 1) {
				throw new NoSuchResident("resident " + name + " does not exist");
	        }
	        
	        UpdateInfoResponse response = new UpdateInfoResponse();
	        response.setResident(getResident(name));
	        return response;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param moveOutResident
	 * @return moveOutResidentResponse
	 * @throws NoSuchResident
	 */

	public MoveOutResidentResponse moveOutResident(MoveOutResident moveOutResident) throws NoSuchResident {
		
		String name = moveOutResident.getResidentName();
		Date movedout = moveOutResident.getMoveOutDate();

		try {
			PreparedStatement st = getConnection().prepareStatement(
					  "UPDATE residents SET movedout = ? WHERE name = ?"
			);
	        st.setDate(1, new java.sql.Date(movedout.getTime()));
	        st.setString(2, name);
	        st.execute();
	        if (st.getUpdateCount() < 1) {
				throw new NoSuchResident("resident " + name + " does not exist");
	        }
	        
	        MoveOutResidentResponse response = new MoveOutResidentResponse();
	        response.setResident(getResident(name));
	        return response;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param addResident
	 * @return addResidentResponse
	 * @throws ResidentAlreadyExists
	 */

	public AddResidentResponse addResident(AddResident addResident) throws ResidentAlreadyExists {
		Resident requestResident = addResident.getResident();
		
		String name = requestResident.getName();
		if (getResident(name) != null) {
			throw new ResidentAlreadyExists("resident " + name + " already exists");
		}
		Date movedin = requestResident.getMovedIn();
		Date movedout = requestResident.getMovedOut();
		Info info = requestResident.getInfo();

		try {
			PreparedStatement st = getConnection().prepareStatement(
					  " INSERT INTO	residents (name, movedin, movedout, roomnumber, info) "
					+ "	VALUES	(?, ?, ?, ?, ?) "
			);
			st.setString(1, name);
	        st.setDate(2, new java.sql.Date(movedin.getTime()));
	        st.setDate(3, movedout == null ? null : new java.sql.Date(movedout.getTime()));
	        st.setInt(4, requestResident.getRoomNumber());
	        st.setString(5, info == null ? null : info.getInfo());
	        st.execute();
	        
	        AddResidentResponse response = new AddResidentResponse();
	        response.setResident(requestResident);
	        return response;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getResident
	 * @return getResidentResponse
	 * @throws NoSuchResident
	 */

	public GetResidentResponse getResident(GetResident getResident) throws NoSuchResident {
		
		String name = getResident.getResidentName();
		Resident resident = getResident(name);
		if (resident == null) {
			throw new NoSuchResident("resident " + name + " does not exist.");
		}
		GetResidentResponse response = new GetResidentResponse();
		response.setResident(resident);
		return response;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param updateRoomNumber
	 * @return updateRoomNumberResponse
	 * @throws NoSuchResident
	 */

	public UpdateRoomNumberResponse updateRoomNumber(UpdateRoomNumber updateRoomNumber) throws NoSuchResident {
		
		String name = updateRoomNumber.getResidentName();
		int roomnumber = updateRoomNumber.getRoomNumber();

		try {
			PreparedStatement st = getConnection().prepareStatement(
					  "UPDATE residents SET roomnumber = ? WHERE name = ? "
			);
	        st.setInt(1, roomnumber);
	        st.setString(2, name);
	        st.execute();
	        if (st.getUpdateCount() < 1) {
				throw new NoSuchResident("resident " + name + " does not exist");
	        }
	        
	        UpdateRoomNumberResponse response = new UpdateRoomNumberResponse();
	        response.setResident(getResident(name));
	        return response;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param isResident
	 * @return isResidentResponse
	 */

	public IsResidentResponse isResident(IsResident isResident) {
		IsResidentResponse response = new IsResidentResponse();
		response.setIsResident(getResident(isResident.getResidentName()) != null);
		return response;
	}

	
	private Resident getResident(String name) {
		try {
			PreparedStatement st = getConnection().prepareStatement("SELECT * FROM residents WHERE name = ?");
	        st.setString(1, name);
	        ResultSet rs = st.executeQuery();
	        if (!rs.next()) {
	    		return null;
	        }
	        return populateResident(rs);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	private Resident populateResident(ResultSet rs) {
		try {
	        Resident r = new Resident();
	        // Name
	        r.setName(rs.getString("name"));
	        // Moved in
	        Date r_movedin = new Date();
	        r_movedin.setTime(rs.getDate("movedin").getTime());
	        r.setMovedIn(r_movedin);
	        // Moved out
	        Date r_movedout = new Date();
	        if (rs.getDate("movedout") != null) {
	        	r_movedout.setTime(rs.getDate("movedout").getTime());
	        	r.setMovedOut(r_movedout);
	        }
	        // Room number
	        r.setRoomNumber(rs.getInt("roomnumber"));
	        // Info
	        String info = rs.getString("info");
	        if (info != null) {
		        Info r_info = new Info();
	        	r_info.setInfo(info);
	        	r.setInfo(r_info);
	        }
	        return r;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
