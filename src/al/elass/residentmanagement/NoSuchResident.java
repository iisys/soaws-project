
/**
 * NoSuchResident.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */

package al.elass.residentmanagement;

public class NoSuchResident extends java.lang.Exception{

    private static final long serialVersionUID = 1459193661464L;
    
    private al.elass.residentmanagement.NoSuchResidentFault faultMessage;

    
        public NoSuchResident() {
            super("NoSuchResident");
        }

        public NoSuchResident(java.lang.String s) {
           super(s);
        }

        public NoSuchResident(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public NoSuchResident(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(al.elass.residentmanagement.NoSuchResidentFault msg){
       faultMessage = msg;
    }
    
    public al.elass.residentmanagement.NoSuchResidentFault getFaultMessage(){
       return faultMessage;
    }
}
    