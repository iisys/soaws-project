
/**
 * ResidentManagementServiceMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */
        package al.elass.residentmanagement;

        /**
        *  ResidentManagementServiceMessageReceiverInOut message receiver
        */

        public class ResidentManagementServiceMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        ResidentManagementServiceSkeleton skel = (ResidentManagementServiceSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)){


        

            if("getResidents".equals(methodName)){
                
                al.elass.residentmanagement.GetResidentsResponse getResidentsResponse29 = null;
	                        al.elass.residentmanagement.GetResidents wrappedParam =
                                                             (al.elass.residentmanagement.GetResidents)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    al.elass.residentmanagement.GetResidents.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getResidentsResponse29 =
                                                   
                                                   
                                                         skel.getResidents(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getResidentsResponse29, false, new javax.xml.namespace.QName("http://elass.al/ResidentManagement/",
                                                    "getResidents"));
                                    } else 

            if("updateInfo".equals(methodName)){
                
                al.elass.residentmanagement.UpdateInfoResponse updateInfoResponse31 = null;
	                        al.elass.residentmanagement.UpdateInfo wrappedParam =
                                                             (al.elass.residentmanagement.UpdateInfo)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    al.elass.residentmanagement.UpdateInfo.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               updateInfoResponse31 =
                                                   
                                                   
                                                         skel.updateInfo(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), updateInfoResponse31, false, new javax.xml.namespace.QName("http://elass.al/ResidentManagement/",
                                                    "updateInfo"));
                                    } else 

            if("moveOutResident".equals(methodName)){
                
                al.elass.residentmanagement.MoveOutResidentResponse moveOutResidentResponse33 = null;
	                        al.elass.residentmanagement.MoveOutResident wrappedParam =
                                                             (al.elass.residentmanagement.MoveOutResident)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    al.elass.residentmanagement.MoveOutResident.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               moveOutResidentResponse33 =
                                                   
                                                   
                                                         skel.moveOutResident(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), moveOutResidentResponse33, false, new javax.xml.namespace.QName("http://elass.al/ResidentManagement/",
                                                    "moveOutResident"));
                                    } else 

            if("addResident".equals(methodName)){
                
                al.elass.residentmanagement.AddResidentResponse addResidentResponse35 = null;
	                        al.elass.residentmanagement.AddResident wrappedParam =
                                                             (al.elass.residentmanagement.AddResident)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    al.elass.residentmanagement.AddResident.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               addResidentResponse35 =
                                                   
                                                   
                                                         skel.addResident(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), addResidentResponse35, false, new javax.xml.namespace.QName("http://elass.al/ResidentManagement/",
                                                    "addResident"));
                                    } else 

            if("getResident".equals(methodName)){
                
                al.elass.residentmanagement.GetResidentResponse getResidentResponse37 = null;
	                        al.elass.residentmanagement.GetResident wrappedParam =
                                                             (al.elass.residentmanagement.GetResident)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    al.elass.residentmanagement.GetResident.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getResidentResponse37 =
                                                   
                                                   
                                                         skel.getResident(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getResidentResponse37, false, new javax.xml.namespace.QName("http://elass.al/ResidentManagement/",
                                                    "getResident"));
                                    } else 

            if("updateRoomNumber".equals(methodName)){
                
                al.elass.residentmanagement.UpdateRoomNumberResponse updateRoomNumberResponse39 = null;
	                        al.elass.residentmanagement.UpdateRoomNumber wrappedParam =
                                                             (al.elass.residentmanagement.UpdateRoomNumber)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    al.elass.residentmanagement.UpdateRoomNumber.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               updateRoomNumberResponse39 =
                                                   
                                                   
                                                         skel.updateRoomNumber(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), updateRoomNumberResponse39, false, new javax.xml.namespace.QName("http://elass.al/ResidentManagement/",
                                                    "updateRoomNumber"));
                                    } else 

            if("isResident".equals(methodName)){
                
                al.elass.residentmanagement.IsResidentResponse isResidentResponse41 = null;
	                        al.elass.residentmanagement.IsResident wrappedParam =
                                                             (al.elass.residentmanagement.IsResident)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    al.elass.residentmanagement.IsResident.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               isResidentResponse41 =
                                                   
                                                   
                                                         skel.isResident(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), isResidentResponse41, false, new javax.xml.namespace.QName("http://elass.al/ResidentManagement/",
                                                    "isResident"));
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        } catch (NoSuchResident e) {

            msgContext.setProperty(org.apache.axis2.Constants.FAULT_NAME,"NoSuchResidentFault");
            org.apache.axis2.AxisFault f = createAxisFault(e);
            if (e.getFaultMessage() != null){
                f.setDetail(toOM(e.getFaultMessage(),false));
            }
            throw f;
            }
         catch (ResidentAlreadyExists e) {

            msgContext.setProperty(org.apache.axis2.Constants.FAULT_NAME,"ResidentAlreadyExistsFault");
            org.apache.axis2.AxisFault f = createAxisFault(e);
            if (e.getFaultMessage() != null){
                f.setDetail(toOM(e.getFaultMessage(),false));
            }
            throw f;
            }
        
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(al.elass.residentmanagement.GetResidents param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.residentmanagement.GetResidents.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.residentmanagement.GetResidentsResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.residentmanagement.GetResidentsResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.residentmanagement.UpdateInfo param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.residentmanagement.UpdateInfo.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.residentmanagement.UpdateInfoResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.residentmanagement.UpdateInfoResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.residentmanagement.NoSuchResidentFault param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.residentmanagement.NoSuchResidentFault.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.residentmanagement.MoveOutResident param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.residentmanagement.MoveOutResident.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.residentmanagement.MoveOutResidentResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.residentmanagement.MoveOutResidentResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.residentmanagement.AddResident param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.residentmanagement.AddResident.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.residentmanagement.AddResidentResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.residentmanagement.AddResidentResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.residentmanagement.ResidentAlreadyExistsFault param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.residentmanagement.ResidentAlreadyExistsFault.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.residentmanagement.GetResident param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.residentmanagement.GetResident.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.residentmanagement.GetResidentResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.residentmanagement.GetResidentResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.residentmanagement.UpdateRoomNumber param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.residentmanagement.UpdateRoomNumber.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.residentmanagement.UpdateRoomNumberResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.residentmanagement.UpdateRoomNumberResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.residentmanagement.IsResident param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.residentmanagement.IsResident.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.residentmanagement.IsResidentResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.residentmanagement.IsResidentResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, al.elass.residentmanagement.GetResidentsResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(al.elass.residentmanagement.GetResidentsResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private al.elass.residentmanagement.GetResidentsResponse wrapGetResidents(){
                                al.elass.residentmanagement.GetResidentsResponse wrappedElement = new al.elass.residentmanagement.GetResidentsResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, al.elass.residentmanagement.UpdateInfoResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(al.elass.residentmanagement.UpdateInfoResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private al.elass.residentmanagement.UpdateInfoResponse wrapUpdateInfo(){
                                al.elass.residentmanagement.UpdateInfoResponse wrappedElement = new al.elass.residentmanagement.UpdateInfoResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, al.elass.residentmanagement.MoveOutResidentResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(al.elass.residentmanagement.MoveOutResidentResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private al.elass.residentmanagement.MoveOutResidentResponse wrapMoveOutResident(){
                                al.elass.residentmanagement.MoveOutResidentResponse wrappedElement = new al.elass.residentmanagement.MoveOutResidentResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, al.elass.residentmanagement.AddResidentResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(al.elass.residentmanagement.AddResidentResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private al.elass.residentmanagement.AddResidentResponse wrapAddResident(){
                                al.elass.residentmanagement.AddResidentResponse wrappedElement = new al.elass.residentmanagement.AddResidentResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, al.elass.residentmanagement.GetResidentResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(al.elass.residentmanagement.GetResidentResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private al.elass.residentmanagement.GetResidentResponse wrapGetResident(){
                                al.elass.residentmanagement.GetResidentResponse wrappedElement = new al.elass.residentmanagement.GetResidentResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, al.elass.residentmanagement.UpdateRoomNumberResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(al.elass.residentmanagement.UpdateRoomNumberResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private al.elass.residentmanagement.UpdateRoomNumberResponse wrapUpdateRoomNumber(){
                                al.elass.residentmanagement.UpdateRoomNumberResponse wrappedElement = new al.elass.residentmanagement.UpdateRoomNumberResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, al.elass.residentmanagement.IsResidentResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(al.elass.residentmanagement.IsResidentResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private al.elass.residentmanagement.IsResidentResponse wrapIsResident(){
                                al.elass.residentmanagement.IsResidentResponse wrappedElement = new al.elass.residentmanagement.IsResidentResponse();
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (al.elass.residentmanagement.AddResident.class.equals(type)){
                
                        return al.elass.residentmanagement.AddResident.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.residentmanagement.AddResidentResponse.class.equals(type)){
                
                        return al.elass.residentmanagement.AddResidentResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.residentmanagement.GetResident.class.equals(type)){
                
                        return al.elass.residentmanagement.GetResident.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.residentmanagement.GetResidentResponse.class.equals(type)){
                
                        return al.elass.residentmanagement.GetResidentResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.residentmanagement.GetResidents.class.equals(type)){
                
                        return al.elass.residentmanagement.GetResidents.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.residentmanagement.GetResidentsResponse.class.equals(type)){
                
                        return al.elass.residentmanagement.GetResidentsResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.residentmanagement.IsResident.class.equals(type)){
                
                        return al.elass.residentmanagement.IsResident.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.residentmanagement.IsResidentResponse.class.equals(type)){
                
                        return al.elass.residentmanagement.IsResidentResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.residentmanagement.MoveOutResident.class.equals(type)){
                
                        return al.elass.residentmanagement.MoveOutResident.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.residentmanagement.MoveOutResidentResponse.class.equals(type)){
                
                        return al.elass.residentmanagement.MoveOutResidentResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.residentmanagement.NoSuchResidentFault.class.equals(type)){
                
                        return al.elass.residentmanagement.NoSuchResidentFault.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.residentmanagement.ResidentAlreadyExistsFault.class.equals(type)){
                
                        return al.elass.residentmanagement.ResidentAlreadyExistsFault.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.residentmanagement.UpdateInfo.class.equals(type)){
                
                        return al.elass.residentmanagement.UpdateInfo.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.residentmanagement.UpdateInfoResponse.class.equals(type)){
                
                        return al.elass.residentmanagement.UpdateInfoResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.residentmanagement.UpdateRoomNumber.class.equals(type)){
                
                        return al.elass.residentmanagement.UpdateRoomNumber.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.residentmanagement.UpdateRoomNumberResponse.class.equals(type)){
                
                        return al.elass.residentmanagement.UpdateRoomNumberResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    