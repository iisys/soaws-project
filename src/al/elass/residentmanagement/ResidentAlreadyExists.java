
/**
 * ResidentAlreadyExists.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */

package al.elass.residentmanagement;

public class ResidentAlreadyExists extends java.lang.Exception{

    private static final long serialVersionUID = 1459193661513L;
    
    private al.elass.residentmanagement.ResidentAlreadyExistsFault faultMessage;

    
        public ResidentAlreadyExists() {
            super("ResidentAlreadyExists");
        }

        public ResidentAlreadyExists(java.lang.String s) {
           super(s);
        }

        public ResidentAlreadyExists(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public ResidentAlreadyExists(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(al.elass.residentmanagement.ResidentAlreadyExistsFault msg){
       faultMessage = msg;
    }
    
    public al.elass.residentmanagement.ResidentAlreadyExistsFault getFaultMessage(){
       return faultMessage;
    }
}
    