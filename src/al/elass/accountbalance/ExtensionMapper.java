
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:04:10 GMT)
 */

        
            package al.elass.accountbalance;
        
            /**
            *  ExtensionMapper class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://elass.al/AccountBalance/".equals(namespaceURI) &&
                  "Shares_type0".equals(typeName)){
                   
                            return  al.elass.accountbalance.Shares_type0.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://elass.al/AccountBalance/".equals(namespaceURI) &&
                  "Entity".equals(typeName)){
                   
                            return  al.elass.accountbalance.Entity.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://elass.al/AccountBalance/".equals(namespaceURI) &&
                  "Mutations".equals(typeName)){
                   
                            return  al.elass.accountbalance.Mutations.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://elass.al/AccountBalance/".equals(namespaceURI) &&
                  "MutationsRequestDetails".equals(typeName)){
                   
                            return  al.elass.accountbalance.MutationsRequestDetails.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://elass.al/AccountBalance/".equals(namespaceURI) &&
                  "Share_type0".equals(typeName)){
                   
                            return  al.elass.accountbalance.Share_type0.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://elass.al/AccountBalance/".equals(namespaceURI) &&
                  "Mutation".equals(typeName)){
                   
                            return  al.elass.accountbalance.Mutation.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://elass.al/AccountBalance/".equals(namespaceURI) &&
                  "Balances".equals(typeName)){
                   
                            return  al.elass.accountbalance.Balances.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://elass.al/AccountBalance/".equals(namespaceURI) &&
                  "Balance".equals(typeName)){
                   
                            return  al.elass.accountbalance.Balance.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://elass.al/AccountBalance/".equals(namespaceURI) &&
                  "MutationEntry".equals(typeName)){
                   
                            return  al.elass.accountbalance.MutationEntry.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    