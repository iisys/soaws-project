
/**
 * NoSuchEntity.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */

package al.elass.accountbalance;

public class NoSuchEntity extends java.lang.Exception{

    private static final long serialVersionUID = 1460283655694L;
    
    private al.elass.accountbalance.NoSuchEntityFault faultMessage;

    
        public NoSuchEntity() {
            super("NoSuchEntity");
        }

        public NoSuchEntity(java.lang.String s) {
           super(s);
        }

        public NoSuchEntity(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public NoSuchEntity(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(al.elass.accountbalance.NoSuchEntityFault msg){
       faultMessage = msg;
    }
    
    public al.elass.accountbalance.NoSuchEntityFault getFaultMessage(){
       return faultMessage;
    }
}
    