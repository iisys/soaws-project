/**
 * AccountBalanceServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */
package al.elass.accountbalance;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import org.apache.axis2.context.ServiceContext;
import org.apache.axis2.service.Lifecycle;

import al.elass.accountbalancecallback.AccountBalanceCallbackServiceStub;
import al.elass.accountbalancecallback.AccountBalanceCallbackServiceStub.Verification;

/**
 * AccountBalanceServiceSkeleton java skeleton for the axisService
 */
public class AccountBalanceServiceSkeleton {

	private final static String HOSTNAME = "127.0.0.1";
	private final static String PORT = "5432";
	private final static String USERNAME = "postgres";
	private final static String PASSWORD = "root";
	private final static String DATABASE = "soaws";
	private static Connection connection;

	private static Connection getConnection() {
		if (connection == null) {
			try {
				Class.forName("org.postgresql.Driver");
			} catch (ClassNotFoundException e) {
				System.out.println("Error loading driver: " + e);
			}

			String url = "jdbc:postgresql://" + HOSTNAME + ":" + PORT + "/" + DATABASE;

			try {
				connection = DriverManager.getConnection(url, USERNAME, PASSWORD);
			} catch (SQLException e) {
				e.printStackTrace();
				// System.exit(1);
				return null;
			}
		}
		return connection;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getBalance
	 * @return getBalanceResponse
	 * @throws GetBalanceFault
	 */

	public GetBalanceResponse getBalance(GetBalance getBalance) throws NoSuchEntity {

		Entity entity = getBalance.getEntity();
		Float balance = getBalance(entity.getEntity());
		if (balance == null) {
			throw new NoSuchEntity("entity " + entity + " does not exist");
		}

		Balance eBalance = new Balance();
		eBalance.setEntity(entity);
		eBalance.setValue(balance.floatValue());

		GetBalanceResponse response = new GetBalanceResponse();
		response.setBalance(eBalance);

		return response;
	}
	
    /**
     * Auto generated method signature
     * 
     * @param isEntity 
     * @return isEntityResponse 
     */
    public IsEntityResponse isEntity(IsEntity isEntity) {
    	IsEntityResponse response = new IsEntityResponse();
    	response.setIsEntity(getEntity(isEntity.getEntity().getEntity()) != null);
    	return response;
    }

	/**
	 * Auto generated method signature
	 * 
	 * @param addEntity
	 * @return addEntityResponse
	 * @throws AddEntityFault
	 */

	public AddEntityResponse addEntity(AddEntity addEntity) {
		String entity = addEntity.getEntity().getEntity();
		
		// Return the existing entity if it's found.
		Entity existingEntity = getEntity(entity);
		if (existingEntity != null) {
			AddEntityResponse response = new AddEntityResponse();
			response.setEntity(existingEntity);
			return response;
		}

		try {
			PreparedStatement st = getConnection().prepareStatement(
					" INSERT INTO entities (entity) VALUES (?) ");
			st.setString(1, entity);
			st.execute();

			Entity e = new Entity();
			e.setEntity(entity);
			AddEntityResponse response = new AddEntityResponse();
			response.setEntity(e);
			return response;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param addMutation
	 * @return addMutationResponse
	 * @throws AddMutationFault
	 */

	public AddMutationResponse addMutation(AddMutation addMutation) throws NoSuchEntity {

		MutationEntry mutationEntry = addMutation.getEntry();
		String description = mutationEntry.getDescription();
		String mutatorEntity = mutationEntry.getMutatedBy().getEntity();
		float totalValue = mutationEntry.getValue();
		Share_type0[] shares = mutationEntry.getShares().getShare();
		Timestamp transactionTimestamp = new Timestamp(new Date().getTime());

		boolean autoCommitValue = true;
		PreparedStatement st = null;
		String faultyEntity = null;
		try {
			autoCommitValue = getConnection().getAutoCommit();
			getConnection().setAutoCommit(false); // Start transaction.

			// First prepare statement for the mutator.
			faultyEntity = mutatorEntity;
			st = getConnection().prepareStatement(
				  " INSERT INTO mutations (entity, value, description, timestmp) "
				+ " VALUES (?, ?, ?, ?) ");
			st.setString(1, mutatorEntity);
			st.setFloat(2, totalValue);
			st.setString(3, description);
			st.setTimestamp(4, transactionTimestamp);
			st.execute();

			st = getConnection().prepareStatement("UPDATE entities SET balance = balance + ? WHERE entity = ?");
			st.setFloat(1, totalValue);
			st.setString(2, mutatorEntity);
			st.execute();

			// First we need to know the sum of the portions.
			float portionsSum = 0;
			for (int i = 0; shares != null && i < shares.length; i++) {
				portionsSum += shares[i].getPortion();
			}

			// Then loop through all the mutatees.
			// If a mutatee entity doesn't exist, the SQL sees a constraint
			// violation and throws an exception.
			for (int s = 0, i = 2; shares != null && s < shares.length; s++, i = i + 2) {
				String mutateeEntity = shares[s].getEntity().getEntity();
				float mutationValue = -1 * totalValue * shares[s].getPortion() / portionsSum;

				faultyEntity = mutateeEntity;
				st = getConnection().prepareStatement(
					  " INSERT INTO mutations (entity, value, description, timestmp, otherentity) "
					+ " VALUES (?, ?, ?, ?, ?) ");
				st.setString(1, mutateeEntity);
				st.setFloat(2, mutationValue);
				st.setString(3, description); // transaction ID part 1
				st.setTimestamp(4, transactionTimestamp); // transaction ID part 2
				st.setString(5, mutatorEntity);
				st.execute();

				st = getConnection().prepareStatement(
					"UPDATE entities SET balance = balance + ? WHERE entity = ?"
				);
				st.setFloat(1, mutationValue);
				st.setString(2, mutateeEntity);
				st.execute();
			}
			getConnection().commit();
			getConnection().setAutoCommit(autoCommitValue);

		} catch (SQLException e) {
			try {
				getConnection().rollback();
				getConnection().setAutoCommit(autoCommitValue);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			throw new NoSuchEntity("entity " + faultyEntity + " does not exist");
		}

		AddMutationResponse response = new AddMutationResponse();
		
		// Format response: entity and balance value.
		Balance balance = new Balance();
		balance.setEntity(mutationEntry.getMutatedBy());
		balance.setValue(getBalance(mutatorEntity).floatValue());
		
		// Format response: last mutation timestamp.
		Calendar lmts = Calendar.getInstance();
		lmts.setTimeInMillis(transactionTimestamp.getTime());
		balance.setLastMutationDate(lmts);

		response.setBalance(balance);
		return response;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getBalances
	 * @return getBalancesResponse
	 */

	public GetBalancesResponse getBalances(GetBalances getBalances) {
		try {
			ResultSet rs = getConnection().prepareStatement("SELECT * FROM entities").executeQuery();

			Balances balances = new Balances();

			while (rs.next()) {
				Entity eEntity = new Entity();
				eEntity.setEntity(rs.getString("entity"));

				Balance balance = new Balance();
				balance.setEntity(eEntity);
				balance.setValue(rs.getFloat("balance"));

				balances.addBalance(balance);
			}

			GetBalancesResponse response = new GetBalancesResponse();
			response.setBalances(balances);
			return response;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getMutations
	 * @return getMutationsResponse
	 * @throws GetMutationsFault
	 */

	public GetMutationsResponse getMutations(GetMutations getMutations) throws NoSuchEntity {

		Entity entity = getMutations.getRequestDetails().getEntity();
		Calendar afterDate = getMutations.getRequestDetails().getAfterDate();
		Calendar beforeDate = getMutations.getRequestDetails().getBeforeDate();

		if (entity != null) {
			if (getEntity(entity.getEntity()) == null) {
				throw new NoSuchEntity("entity " + entity + " does not exist");
			}
		}

		try {
			PreparedStatement st = getConnection().prepareStatement(
				  "SELECT * FROM mutations WHERE timestmp > ? AND timestmp < ? "
				+ (entity != null ? " AND entity = ? " : "") 
				+ " ORDER BY timestmp DESC ");
			st.setTimestamp(1, new java.sql.Timestamp(afterDate.getTimeInMillis()));
			st.setTimestamp(2, new java.sql.Timestamp(beforeDate.getTimeInMillis()));
			if (entity != null) {
				st.setString(3, entity.getEntity());
			}
			ResultSet rs = st.executeQuery();

			Mutations mutations = new Mutations();
			while (rs.next()) {

				Entity mutateeEntity = new Entity();
				mutateeEntity.setEntity(rs.getString("entity"));

				Mutation mutation = new Mutation();
				mutation.setMutatee(mutateeEntity);
				mutation.setMutationValue(rs.getFloat("value"));
				mutation.setDescription(rs.getString("description"));
				Calendar timestmp = Calendar.getInstance();
				timestmp.setTimeInMillis(rs.getTimestamp("timestmp").getTime());
				mutation.setDate(timestmp);

				// Per mutation, get the value of the original transaction
				// mutation it this isn't
				// already the original.
				if (rs.getString("otherentity") != null) {
					PreparedStatement st1 = getConnection().prepareStatement(
						" SELECT value FROM mutations WHERE timestmp = ? AND description = ? AND otherentity IS NULL ");
					st1.setTimestamp(1, rs.getTimestamp("timestmp"));
					st1.setString(2, rs.getString("description"));
					ResultSet rs1 = st1.executeQuery();
					if (!rs1.next()) {
						throw new RuntimeException("inconsistency!");
					}
					mutation.setTotalValue(rs1.getFloat("value"));

					Entity mutatorEntity = new Entity();
					mutatorEntity.setEntity(rs.getString("otherentity"));
					mutation.setMutator(mutatorEntity);
				} else {
					mutation.setTotalValue(rs.getFloat("value"));
					mutation.setMutator(mutateeEntity);
				}

				mutations.addMutation(mutation);
			}

			GetMutationsResponse response = new GetMutationsResponse();
			response.setMutations(mutations);
			return response;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param verifyBalances8
	 * @return
	 */

	public void verifyBalances(VerifyBalances verifyBalances8) {
		Verification response = new Verification();
		response.setResult(true);
		response.setTransID(verifyBalances8.getTransID());
		
		try {
			PreparedStatement st = getConnection().prepareStatement(
				  " SELECT	e.entity, "
				+ "		   	   ((COUNT(m.value) = 0 AND e.balance = 0) OR "
				+ "				(COUNT(m.value) > 0 AND e.balance = SUM(m.value))) "
				+ "			as consistent "
				+ " FROM	entities e "
				+ " LEFT JOIN mutations m ON e.entity = m.entity "
				+ " GROUP BY e.entity "
			);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				if (!rs.getBoolean("consistent")) {
					response.setResult(false);
				}
			}
			// This operation might, in theory, take a while. So we add a slight delay to simulate that.
			Thread.sleep(5000);
		} catch (SQLException|InterruptedException e) {
			e.printStackTrace();
		}
		
		try {
			new AccountBalanceCallbackServiceStub().receiveVerificationResult(response);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	private Entity getEntity(String entity) {
		try {
			PreparedStatement st = getConnection().prepareStatement("SELECT * FROM entities WHERE entity = ?");
			st.setString(1, entity);
			ResultSet rs = st.executeQuery();
			if (!rs.next()) {
				return null;
			}
			Entity e = new Entity();
			e.setEntity(rs.getString("entity"));
			return e;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	private Float getBalance(String entity) {
		try {
			PreparedStatement st = getConnection().prepareStatement("SELECT balance FROM entities WHERE entity = ?");
			st.setString(1, entity);
			ResultSet rs = st.executeQuery();
			return !rs.next() ? null : rs.getFloat("balance");
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

}
