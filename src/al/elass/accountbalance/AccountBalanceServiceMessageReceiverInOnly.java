

/**
 * AccountBalanceServiceMessageReceiverInOnly.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */
        package al.elass.accountbalance;

        /**
        *  AccountBalanceServiceMessageReceiverInOnly message receiver
        */

        public class AccountBalanceServiceMessageReceiverInOnly extends org.apache.axis2.receivers.AbstractInMessageReceiver{

        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext inMessage) throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(inMessage);

        AccountBalanceServiceSkeleton skel = (AccountBalanceServiceSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = inMessage.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)){

        
            if("verifyBalances".equals(methodName)){
            
            al.elass.accountbalance.VerifyBalances wrappedParam = (al.elass.accountbalance.VerifyBalances)fromOM(
                                                        inMessage.getEnvelope().getBody().getFirstElement(),
                                                        al.elass.accountbalance.VerifyBalances.class,
                                                        getEnvelopeNamespaces(inMessage.getEnvelope()));
                                            
                                                     skel.verifyBalances(wrappedParam);
                                                
                } else {
                  throw new java.lang.RuntimeException("method not found");
                }
            

        }
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }


        
        //
            private  org.apache.axiom.om.OMElement  toOM(al.elass.accountbalance.AddEntity param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.accountbalance.AddEntity.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.accountbalance.AddEntityResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.accountbalance.AddEntityResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.accountbalance.GetBalance param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.accountbalance.GetBalance.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.accountbalance.GetBalanceResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.accountbalance.GetBalanceResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.accountbalance.NoSuchEntityFault param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.accountbalance.NoSuchEntityFault.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.accountbalance.GetMutations param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.accountbalance.GetMutations.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.accountbalance.GetMutationsResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.accountbalance.GetMutationsResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.accountbalance.VerifyBalances param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.accountbalance.VerifyBalances.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.accountbalance.GetBalances param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.accountbalance.GetBalances.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.accountbalance.GetBalancesResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.accountbalance.GetBalancesResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.accountbalance.AddMutation param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.accountbalance.AddMutation.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.accountbalance.AddMutationResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.accountbalance.AddMutationResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.accountbalance.IsEntity param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.accountbalance.IsEntity.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.accountbalance.IsEntityResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.accountbalance.IsEntityResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, al.elass.accountbalance.AddEntityResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(al.elass.accountbalance.AddEntityResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private al.elass.accountbalance.AddEntityResponse wrapAddEntity(){
                                al.elass.accountbalance.AddEntityResponse wrappedElement = new al.elass.accountbalance.AddEntityResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, al.elass.accountbalance.GetBalanceResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(al.elass.accountbalance.GetBalanceResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private al.elass.accountbalance.GetBalanceResponse wrapGetBalance(){
                                al.elass.accountbalance.GetBalanceResponse wrappedElement = new al.elass.accountbalance.GetBalanceResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, al.elass.accountbalance.GetMutationsResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(al.elass.accountbalance.GetMutationsResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private al.elass.accountbalance.GetMutationsResponse wrapGetMutations(){
                                al.elass.accountbalance.GetMutationsResponse wrappedElement = new al.elass.accountbalance.GetMutationsResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, al.elass.accountbalance.GetBalancesResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(al.elass.accountbalance.GetBalancesResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private al.elass.accountbalance.GetBalancesResponse wrapGetBalances(){
                                al.elass.accountbalance.GetBalancesResponse wrappedElement = new al.elass.accountbalance.GetBalancesResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, al.elass.accountbalance.AddMutationResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(al.elass.accountbalance.AddMutationResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private al.elass.accountbalance.AddMutationResponse wrapAddMutation(){
                                al.elass.accountbalance.AddMutationResponse wrappedElement = new al.elass.accountbalance.AddMutationResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, al.elass.accountbalance.IsEntityResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(al.elass.accountbalance.IsEntityResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private al.elass.accountbalance.IsEntityResponse wrapIsEntity(){
                                al.elass.accountbalance.IsEntityResponse wrappedElement = new al.elass.accountbalance.IsEntityResponse();
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (al.elass.accountbalance.AddEntity.class.equals(type)){
                
                        return al.elass.accountbalance.AddEntity.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.accountbalance.AddEntityResponse.class.equals(type)){
                
                        return al.elass.accountbalance.AddEntityResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.accountbalance.AddMutation.class.equals(type)){
                
                        return al.elass.accountbalance.AddMutation.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.accountbalance.AddMutationResponse.class.equals(type)){
                
                        return al.elass.accountbalance.AddMutationResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.accountbalance.GetBalance.class.equals(type)){
                
                        return al.elass.accountbalance.GetBalance.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.accountbalance.GetBalanceResponse.class.equals(type)){
                
                        return al.elass.accountbalance.GetBalanceResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.accountbalance.GetBalances.class.equals(type)){
                
                        return al.elass.accountbalance.GetBalances.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.accountbalance.GetBalancesResponse.class.equals(type)){
                
                        return al.elass.accountbalance.GetBalancesResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.accountbalance.GetMutations.class.equals(type)){
                
                        return al.elass.accountbalance.GetMutations.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.accountbalance.GetMutationsResponse.class.equals(type)){
                
                        return al.elass.accountbalance.GetMutationsResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.accountbalance.IsEntity.class.equals(type)){
                
                        return al.elass.accountbalance.IsEntity.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.accountbalance.IsEntityResponse.class.equals(type)){
                
                        return al.elass.accountbalance.IsEntityResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.accountbalance.NoSuchEntityFault.class.equals(type)){
                
                        return al.elass.accountbalance.NoSuchEntityFault.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.accountbalance.VerifyBalances.class.equals(type)){
                
                        return al.elass.accountbalance.VerifyBalances.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    



        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }



        }//end of class

    