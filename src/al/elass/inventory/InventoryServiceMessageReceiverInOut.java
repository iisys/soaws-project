
/**
 * InventoryServiceMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */
        package al.elass.inventory;

        /**
        *  InventoryServiceMessageReceiverInOut message receiver
        */

        public class InventoryServiceMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        InventoryServiceSkeleton skel = (InventoryServiceSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)){


        

            if("getInventory".equals(methodName)){
                
                al.elass.inventory.GetInventoryResponse getInventoryResponse13 = null;
	                        al.elass.inventory.GetInventory wrappedParam =
                                                             (al.elass.inventory.GetInventory)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    al.elass.inventory.GetInventory.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getInventoryResponse13 =
                                                   
                                                   
                                                         skel.getInventory(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getInventoryResponse13, false, new javax.xml.namespace.QName("http://elass.al/Inventory/",
                                                    "getInventory"));
                                    } else 

            if("updateStock".equals(methodName)){
                
                al.elass.inventory.UpdateStockResponse updateStockResponse15 = null;
	                        al.elass.inventory.UpdateStock wrappedParam =
                                                             (al.elass.inventory.UpdateStock)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    al.elass.inventory.UpdateStock.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               updateStockResponse15 =
                                                   
                                                   
                                                         skel.updateStock(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), updateStockResponse15, false, new javax.xml.namespace.QName("http://elass.al/Inventory/",
                                                    "updateStock"));
                                    } else 

            if("getInventoryItem".equals(methodName)){
                
                al.elass.inventory.GetInventoryItemResponse getInventoryItemResponse17 = null;
	                        al.elass.inventory.GetInventoryItem wrappedParam =
                                                             (al.elass.inventory.GetInventoryItem)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    al.elass.inventory.GetInventoryItem.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getInventoryItemResponse17 =
                                                   
                                                   
                                                         skel.getInventoryItem(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getInventoryItemResponse17, false, new javax.xml.namespace.QName("http://elass.al/Inventory/",
                                                    "getInventoryItem"));
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(al.elass.inventory.GetInventory param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.inventory.GetInventory.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.inventory.GetInventoryResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.inventory.GetInventoryResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.inventory.UpdateStock param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.inventory.UpdateStock.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.inventory.UpdateStockResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.inventory.UpdateStockResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.inventory.GetInventoryItem param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.inventory.GetInventoryItem.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(al.elass.inventory.GetInventoryItemResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(al.elass.inventory.GetInventoryItemResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, al.elass.inventory.GetInventoryResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(al.elass.inventory.GetInventoryResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private al.elass.inventory.GetInventoryResponse wrapGetInventory(){
                                al.elass.inventory.GetInventoryResponse wrappedElement = new al.elass.inventory.GetInventoryResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, al.elass.inventory.UpdateStockResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(al.elass.inventory.UpdateStockResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private al.elass.inventory.UpdateStockResponse wrapUpdateStock(){
                                al.elass.inventory.UpdateStockResponse wrappedElement = new al.elass.inventory.UpdateStockResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, al.elass.inventory.GetInventoryItemResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(al.elass.inventory.GetInventoryItemResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private al.elass.inventory.GetInventoryItemResponse wrapGetInventoryItem(){
                                al.elass.inventory.GetInventoryItemResponse wrappedElement = new al.elass.inventory.GetInventoryItemResponse();
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (al.elass.inventory.GetInventory.class.equals(type)){
                
                        return al.elass.inventory.GetInventory.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.inventory.GetInventoryItem.class.equals(type)){
                
                        return al.elass.inventory.GetInventoryItem.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.inventory.GetInventoryItemResponse.class.equals(type)){
                
                        return al.elass.inventory.GetInventoryItemResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.inventory.GetInventoryResponse.class.equals(type)){
                
                        return al.elass.inventory.GetInventoryResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.inventory.UpdateStock.class.equals(type)){
                
                        return al.elass.inventory.UpdateStock.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (al.elass.inventory.UpdateStockResponse.class.equals(type)){
                
                        return al.elass.inventory.UpdateStockResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    