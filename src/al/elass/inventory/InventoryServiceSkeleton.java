/**
 * InventoryServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */
package al.elass.inventory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * InventoryServiceSkeleton java skeleton for the axisService
 */
public class InventoryServiceSkeleton {
	
	private final static String HOSTNAME = "127.0.0.1";
	private final static String PORT = "5432";
	private final static String USERNAME = "postgres";
	private final static String PASSWORD = "root";
	private final static String DATABASE = "soaws";
	private static Connection connection;
	
	private static Connection getConnection() {
		if (connection == null) {		
	        try {
	            Class.forName("org.postgresql.Driver");
	        } catch (ClassNotFoundException e) {
	            System.out.println("Error loading driver: " + e);
	        }
	        
	        String url = "jdbc:postgresql://" + HOSTNAME + ":" + PORT + "/" + DATABASE;
	        
	        try {
	        	connection = DriverManager.getConnection(url, USERNAME, PASSWORD);
	        } catch (SQLException e) {
	        	e.printStackTrace();
	        	//System.exit(1);
	        	return null;
	        }
		}
        return connection;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getInventory
	 * @return getInventoryResponse
	 */

	public GetInventoryResponse getInventory(GetInventory getInventory) {
		
		GetInventoryResponse response = new GetInventoryResponse();
		ItemList itemList = new ItemList();
		response.setItemList(itemList);
		
		try {
			PreparedStatement st = getConnection().prepareStatement(
				  "SELECT * FROM inventory WHERE stock > 0"
			);
			ResultSet rs = st.executeQuery();
			
			while (rs.next()) {
				Item item = new Item();
				item.setName(rs.getString("item"));
				item.setStock(rs.getInt("stock"));
				item.setPrice(rs.getFloat("price"));
				itemList.addItem(item);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			response = null;
		}
		return response;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param updateStock
	 * @return updateStockResponse
	 */

	public UpdateStockResponse updateStock(UpdateStock updateStock) {

		StockUpdate request = updateStock.getStockUpdate();
		String itemName = request.getItemName();
		int amount = request.getRelativeAmount();
		Float newPrice = request.getNewPrice();
		
		Item item = getItem(itemName);
		
		try {
			if (item != null) {
				// Make sure we don't update the stock to an int below zero.
				int currentAmount = item.getStock();
				if (currentAmount + amount < 0) {
					amount = -1 * currentAmount;
				}
				PreparedStatement st = getConnection().prepareStatement(
					  " UPDATE	inventory "
					+ " SET 	stock = stock + ?"
					+ (request.isNewPriceSpecified() ? ", price = ? " : "")
					+ " WHERE	item = ? "
				);
				st.setInt(1, amount);
				if (request.isNewPriceSpecified()) {
					st.setFloat(2, newPrice);
					st.setString(3, itemName);
					item.setPrice(newPrice);
				} else {
					st.setString(2, itemName);
				}
				st.execute();
				
				item.setStock(item.getStock() + amount);
				
			} else {
				// Make sure we don't update the stock to an int below zero.
				if (amount < 0) {
					amount = 0;
				}
				if (!request.isNewPriceSpecified()) {
					newPrice = 0f;
				}
				PreparedStatement st = getConnection().prepareStatement(
					  " INSERT INTO inventory (item, stock, price) "
					+ " VALUES	(?, ?, ?) "
				);
				st.setString(1, itemName);
				st.setInt(2, amount);
				st.setFloat(3, newPrice);
				st.execute();
				
				item = new Item();
				item.setName(itemName);
				item.setStock(amount);
				item.setPrice(newPrice);
			}
			UpdateStockResponse response = new UpdateStockResponse();
			response.setItem(item);
			return response;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Return the item with its stock and price. If the item doesn't exist, the item is
	 * returned anyway, but with the stock and price set to zero.
	 * 
	 * @param getInventoryItem
	 * @return getInventoryItemResponse1
	 */
	public GetInventoryItemResponse getInventoryItem(GetInventoryItem getInventoryItem) {
		
		String itemName = getInventoryItem.getItemName();
		Item item = getItem(itemName);
		if (item == null) {
			item = new Item();
			item.setName(itemName);
			item.setStock(0);
			item.setPrice(0f);
		}
		GetInventoryItemResponse response = new GetInventoryItemResponse();
		response.setItem(item);
		return response;
	}
	
	private Item getItem(String name) {
		try {
			PreparedStatement st = getConnection().prepareStatement(
				  "SELECT * FROM inventory WHERE item = ?"
			);
			st.setString(1, name);
			ResultSet rs = st.executeQuery();
			
			if (rs.next()) {
				Item item = new Item();
				item.setName(rs.getString("item"));
				item.setStock(rs.getInt("stock"));
				item.setPrice(rs.getFloat("price"));
				return item;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
