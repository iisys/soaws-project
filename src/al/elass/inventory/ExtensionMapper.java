
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:04:10 GMT)
 */

        
            package al.elass.inventory;
        
            /**
            *  ExtensionMapper class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://elass.al/Inventory/".equals(namespaceURI) &&
                  "ItemList".equals(typeName)){
                   
                            return  al.elass.inventory.ItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://elass.al/Inventory/".equals(namespaceURI) &&
                  "StockUpdate".equals(typeName)){
                   
                            return  al.elass.inventory.StockUpdate.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://elass.al/Inventory/".equals(namespaceURI) &&
                  "Item".equals(typeName)){
                   
                            return  al.elass.inventory.Item.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    